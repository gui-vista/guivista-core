package io.gitlab.guiVista.core.dataType.variant

import io.gitlab.guiVista.core.Closable

/** A variant that acts as a dictionary (equivalent to Kotlin's Map, and MutableMap data types). */
public expect class VariantDictionary : Closable {
    public companion object {
        /**
         * Creates a [VariantDictionary] which is initialized. You should call [close] when you are finished with the
         * [VariantDictionary] instance. The memory will not be automatically freed by any other call.
         * @return The new [VariantDictionary].
         */
        public fun create(): VariantDictionary
    }

    /**
     * Looks up a value in a [VariantDictionary]. If [key] isn't found in dictionary then *null* is returned. The
     * [expectedType] specifies what type of value is expected. If the value associated with [key] has a different
     * type then *null* is returned. If the [key] is found, and the value has the correct type then it is returned. If
     * [expectedType] was specified then any non null return value will have this type.
     * @param key The key to lookup in the dictionary.
     * @param expectedType A [VariantType] or *null*.
     * @return The value of the dictionary key or *null*.
     */
    public fun lookupValue(key: String, expectedType: VariantType? = null): Variant?

    /**
     * Inserts an entry into a [VariantDictionary].
     * @param key The key use.
     * @param value The value to use.
     */
    public fun insertEntry(key: String, value: String)

    /**
     * Inserts an entry into a [VariantDictionary].
     * @param key The key use.
     * @param value The value to use.
     */
    public fun insertEntry(key: String, value: Variant)

    /**
     * Removes a key and its associated value from a [VariantDictionary].
     * @param key The key to remove.
     * @return A value of *true* if [key] was found, and removed.
     */
    public fun remove(key: String): Boolean

    /**
     * Checks if [key] exists in the dictionary.
     * @param key The key to lookup in the dictionary.
     * @return A value of *true* if the [key] is in the dictionary.
     */
    public operator fun contains(key: String): Boolean
}