package io.gitlab.guiVista.core.dataType

import io.gitlab.guiVista.core.Closable

/**
 * A singly linked list that can be iterated in one direction. Remember to call [close] when you are finished with a
 * SinglyLinkedList instance. Maps to [GSList](https://developer.gnome.org/glib/stable/glib-Singly-Linked-Lists.html)
 * GLib data type.
 */
public expect class SinglyLinkedList : Closable {
    /** The number of elements in a list. */
    public val length: UInt
    public val next: SinglyLinkedList?

    /**
     * The last element in this list, or *null* if the list has no elements. This property iterates over the whole list.
     */
    public val last: SinglyLinkedList?

    public companion object {
        public fun create(): SinglyLinkedList
    }

    public fun copy(): SinglyLinkedList

    /** Reverses the list. */
    public fun reverse(): SinglyLinkedList

    /**
     * Gets the position of the given element in the list (starting from 0).
     * @param listLink An element in the list.
     * @return The position of the element in the list, or *-1* if the element isn't found.
     */
    public fun position(listLink: SinglyLinkedList): Int

    /**
     * Adds a singly linked list onto the end of this list. Note that the elements of the added list are not copied.
     * They are used directly.
     */
    public infix fun concat(list: SinglyLinkedList): SinglyLinkedList

    /**
     * Gets the element at the given [position][pos] in this list. If you intend to iterate over every element then it
     * is better to use the **iterateElements** function.
     * @param pos The position of the element, counting from *0*.
     * @return The element, or *null* if the position is off the end of this list.
     */
    public infix fun elementAt(pos: UInt): SinglyLinkedList?
}
