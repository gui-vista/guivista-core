package io.gitlab.guiVista.core.dataType

import io.gitlab.guiVista.core.Quark

/**
 * Represents a GObject data typeInfo.
 * @param id The data typeInfo identifier.
 */
public expect inline class TypeInfo(public val id: ULong) {
    /**
     * Gets the unique name that is assigned to a typeInfo ID. Note that this function cannot cope with invalid typeInfo IDs.
     * Note that `G_TYPE_INVALID` may be handled by this function, as may be any other validly registered typeInfo ID, but
     * randomized typeInfo IDs will most likely lead to a **crash**!
     * @return The static typeInfo name or *""* (an empty [String]).
     */
    public fun fetchName(): String

    /**
     * Gets the corresponding [Quark] of the typeInfo id's name.
     * @return The [Quark], or a [Quark] with an [id][Quark.id] of *0*.
     */
    public fun fetchQuarkName(): Quark

    public companion object {
        /**
         * Lookup the typeInfo ID from a given [typeInfo name][name], returning a [TypeInfo] with an [id] of *0* if no typeInfo has
         * been registered under this name (this is the preferred method to find out by name whether a specific typeInfo
         * has been registered yet).
         * @param name The typeInfo name to lookup.
         * @return The [TypeInfo], or the [TypeInfo] with an [id] of *0*.
         */
        public fun fromName(name: String): TypeInfo
    }
}
