package io.gitlab.guiVista.core

/** Root foundation for interfaces, and classes based on GObject. */
public expect interface ObjectBase : Closable {
    /**
     * Disconnects a event (signal) from a event handler (slot) on a object.
     * @param handlerId The handler ID to use.
     */
    public open fun disconnectEvent(handlerId: ULong)
}
