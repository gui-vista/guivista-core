package io.gitlab.guiVista.core

/**
 * Provides data binding for GObject properties.
 */
public expect class DataBinding : Closable {
    /**
     * The flags passed when constructing the DataBinding.
     */
    public val flags: UInt

    /**
     * The Object instance used as the source of the binding. A DataBinding can outlive the source Object as
     * the binding does not hold a strong reference to the source. If the source is destroyed before the binding then
     * this property will be *null*.
     */
    public val sourceObj: Object?

    /**
     * The name of the source property.
     */
    public val sourceProperty: String

    /**
     * The Object instance used as the target of the binding. A DataBinding can outlive the target Object as
     * the binding does not hold a strong reference to the target. If the target is destroyed before the binding then
     * this property will be *null*.
     */
    public val targetObj: Object?

    /**
     * The name of the target property.
     */
    public val targetProperty: String

    /**
     * Explicitly releases the binding between the source and the target property expressed by the binding. This
     * function will release the reference that is being held on the binding instance if the binding is still bound;
     * if you want to hold on to the DataBinding instance after calling [unbind], then you will need to hold a
     * reference to it.
     *
     * Note however that this function does not take ownership of binding. It only unrefs the reference that was
     * initially created by `bindProperty` and is owned by the binding.
     */
    public fun unbind()
}
