package io.gitlab.guiVista.core

/** Default implementation of ValueBase. */
public expect class Value : ValueBase {
    public companion object {
        public fun create(type: ULong): Value
    }
}
