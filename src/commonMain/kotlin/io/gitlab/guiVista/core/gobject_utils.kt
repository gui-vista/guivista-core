package io.gitlab.guiVista.core

/** Frees up the empty data reference. */
public expect fun disposeEmptyDataRef()
