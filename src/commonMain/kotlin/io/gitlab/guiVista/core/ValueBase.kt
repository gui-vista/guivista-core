package io.gitlab.guiVista.core

import io.gitlab.guiVista.core.dataType.TypeInfo

/** Represents a GValue, which is used with GObject. */
public expect interface ValueBase : Closable {
    public open val typeInfo: TypeInfo

    public open fun changeString(newValue: String)

    public open fun changeStaticString(newValue: String)

    public open fun changeBoolean(newValue: Boolean)

    public open fun changeStaticChar(newValue: Char)

    public open fun changeDouble(newValue: Double)

    public open fun changeFloat(newValue: Float)

    public open fun changeInt(newValue: Int)

    public open fun changeLong(newValue: Long)

    public open fun changeObject(newValue: Object?)

    public open fun changeUByte(newValue: UByte)

    public open fun changeUInt(newValue: UInt)

    public open fun changeULong(newValue: ULong)

    public open fun fetchString(): String

    public open fun fetchBoolean(): Boolean

    public open fun fetchChar(): Char

    public open fun fetchDouble(): Double

    public open fun fetchFloat(): Float

    public open fun fetchInt(): Int

    public open fun fetchLong(): Long

    public open fun fetchObject(): Object

    public open fun fetchUByte(): UByte

    public open fun fetchUInt(): UInt

    public open fun fetchULong(): ULong
}
