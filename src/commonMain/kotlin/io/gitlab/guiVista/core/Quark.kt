package io.gitlab.guiVista.core

/**
 * A two way association between a [String], and a unique [Integer][Int] identifier.
 * @param id The unique identifier.
 */
public expect inline class Quark(public val id: UInt = 0u) {
    public companion object {
        /**
         * Gets the [Quark] identifying the given [string] If the [string] does not currently have an associated
         * [Quark] then a new [Quark] is created using a copy of the [string].
         * @param string A [String].
         * @return The [Quark] identifying the [string].
         */
        public fun fromString(string: String): Quark

        /**
         * Gets the [Quark] identifying the given [static string][string]. If the [string] does not currently have an
         * associated [Quark] then a new [Quark] is created, which is linked to the given string. Note that this
         * function is identical to [fromString] except that if a new [Quark] is created the [string] itself is used
         * rather than a copy. This saves memory but can only be used if the [string] will continue to exist until the
         * program terminates. It can be used with statically allocated strings in the main program, but not with
         * statically allocated memory in dynamically loaded modules. If you expect to ever unload the module again
         * (e.g. do **NOT** use this function in GTK+ theme engines!).
         * @param string A [String].
         * @return The [Quark] identifying the [string].
         */
        public fun fromStaticString(string: String): Quark

        /**
         * Gets the [Quark] associated with the given [string], or the [Quark] with an [id] of *0* if [string] has no
         * associated [Quark]. If you want the [Quark] to be created if it doesn't already exist then use [fromString],
         * or [fromStaticString].
         * @param string A [String].
         * @return The [Quark] associated with the [string], or the [Quark] with an [id] of *0* if [string] has no
         * [Quark] associated with it.
         */
        public fun tryString(string: String): Quark
    }
}
