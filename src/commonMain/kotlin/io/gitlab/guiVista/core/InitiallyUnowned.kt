package io.gitlab.guiVista.core

public interface InitiallyUnowned : ObjectBase {
    override fun close() {
        // Do nothing since the object isn't owned.
    }
}