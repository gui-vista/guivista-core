package io.gitlab.guiVista.core

/** Default implementation of ObjectBase. */
public expect class Object : ObjectBase {
    public fun fetchProperty(name: String, type: ULong): ValueBase

    public fun changeStaticStringProperty(name: String, value: String)

    public fun changeBooleanProperty(name: String, value: Boolean)

    public fun changeStaticCharProperty(name: String, value: Char)

    public fun changeDoubleProperty(name: String, value: Double)

    public fun changeFloatProperty(name: String, value: Float)

    public fun changeIntProperty(name: String, value: Int)

    public fun changeLongProperty(name: String, value: Long)

    public fun changeObjectProperty(name: String, value: Object?)

    public fun changeUByteProperty(name: String, value: UByte)

    public fun changeUIntProperty(name: String, value: UInt)

    public fun changeULongProperty(name: String, value: ULong)
}
