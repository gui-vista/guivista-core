package io.gitlab.guiVista.core

public actual interface ObjectBase : Closable {
    public actual fun disconnectEvent(handlerId: ULong) {}
}
