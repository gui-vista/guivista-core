package io.gitlab.guiVista.core

import glib2.GValue
import glib2.G_TYPE_OBJECT
import glib2.g_value_init
import kotlinx.cinterop.Arena
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.alloc
import kotlinx.cinterop.ptr

public actual class Value private constructor(
    value: CPointer<GValue>? = null,
    type: ULong = G_TYPE_OBJECT
) : ValueBase {
    private val arena = Arena()
    override val gValuePtr: CPointer<GValue> = value ?: createFreshValue(type)

    private fun createFreshValue(type: ULong): CPointer<GValue> {
        val ptr = arena.alloc<GValue>().ptr
        g_value_init(ptr, type)
        return ptr
    }

    public actual companion object {
        public actual fun create(type: ULong): Value = Value(type = type)

        public fun fromExistingValue(value: CPointer<GValue>?): Value = Value(value)
    }
}
