package io.gitlab.guiVista.core

import glib2.gpointer
import kotlinx.cinterop.*

/**
 * Converts the gpointer into a UTF-8 encoded string.
 */
public val gpointer.stringValue: String
    get() = reinterpret<ByteVar>().toKString()

/**
 * Converts the gpointer into a [Byte].
 */
public val gpointer.byteValue: Byte
    get() = reinterpret<ByteVar>().pointed.value

/**
 * Converts the gpointer into a [Short].
 */
public val gpointer.shortValue: Short
    get() = reinterpret<ShortVar>().pointed.value

/**
 * Converts the gpointer into a [Int].
 */
public val gpointer.intValue: Int
    get() = reinterpret<IntVar>().pointed.value

/**
 * Converts the gpointer into a [Long].
 */
public val gpointer.longValue: Long
    get() = reinterpret<LongVar>().pointed.value

/**
 * Converts the gpointer into a [UByte].
 */
public val gpointer.uByteValue: UByte
    get() = reinterpret<UByteVar>().pointed.value

/**
 * Converts the gpointer into a [UShort].
 */
public val gpointer.uShortValue: UShort
    get() = reinterpret<UShortVar>().pointed.value

/**
 * Converts the gpointer into a [UInt].
 */
public val gpointer.uIntValue: UInt
    get() = reinterpret<UIntVar>().pointed.value

/**
 * Converts the gpointer into a [ULong].
 */
public val gpointer.uLongValue: ULong
    get() = reinterpret<ULongVar>().pointed.value

/**
 * Converts the gpointer into a [Float].
 */
public val gpointer.floatValue: Float
    get() = reinterpret<FloatVar>().pointed.value

/**
 * Converts the gpointer into a [Double].
 */
public val gpointer.doubleValue: Double
    get() = reinterpret<DoubleVar>().pointed.value
