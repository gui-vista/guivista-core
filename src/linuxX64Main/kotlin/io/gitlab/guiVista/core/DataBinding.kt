package io.gitlab.guiVista.core

import glib2.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.toKString

public actual class DataBinding private constructor(ptr: CPointer<GBinding>?) : Closable {
    public val gBindingPtr: CPointer<GBinding>? = ptr

    public actual val flags: UInt
        get() = g_binding_get_flags(gBindingPtr)

    public actual val sourceObj: Object?
        get() {
            val ptr = g_binding_get_source(gBindingPtr)
            return if (ptr != null) Object.fromPointer(ptr) else null
        }

    public actual val targetObj: Object?
        get() {
            val ptr = g_binding_get_target(gBindingPtr)
            return if (ptr != null) Object.fromPointer(ptr) else null
        }

    public actual val sourceProperty: String
        get() = g_binding_get_source_property(gBindingPtr)?.toKString() ?: ""

    public actual val targetProperty: String
        get() = g_binding_get_target_property(gBindingPtr)?.toKString() ?: ""

    public companion object {
        public fun fromPointer(ptr: CPointer<GBinding>?): DataBinding = DataBinding(ptr)
    }

    public actual fun unbind() {
        g_binding_unbind(gBindingPtr)
    }

    override fun close() {
        g_object_unref(gBindingPtr)
    }
}
