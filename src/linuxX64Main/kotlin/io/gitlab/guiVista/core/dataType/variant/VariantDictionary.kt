package io.gitlab.guiVista.core.dataType.variant

import glib2.*
import io.gitlab.guiVista.core.Closable
import kotlinx.cinterop.CPointer

public actual class VariantDictionary private constructor(ptr: CPointer<GVariantDict>? = null) : Closable {
    private val gVariantPtr = if (ptr != null) Variant.create().gVariantPtr else null
    public val gVariantDictPtr: CPointer<GVariantDict>? = ptr ?: g_variant_dict_new(gVariantPtr)

    public actual companion object {
        public fun fromPointer(ptr: CPointer<GVariantDict>?): VariantDictionary = VariantDictionary(ptr)

        public actual fun create(): VariantDictionary = VariantDictionary()
    }

    public actual fun lookupValue(key: String, expectedType: VariantType?): Variant? {
        val ptr = g_variant_dict_lookup_value(
            dict = gVariantDictPtr,
            key = key,
            expected_type = expectedType?.gVariantTypePtr
        )
        return if (ptr != null) Variant.fromPointer(ptr) else null
    }

    public actual fun insertEntry(key: String, value: String) {
        g_variant_dict_insert(dict = gVariantDictPtr, key = key, format_string = value)
    }

    public actual fun insertEntry(key: String, value: Variant) {
        g_variant_dict_insert_value(dict = gVariantDictPtr, key = key, value = value.gVariantPtr)
    }

    public actual fun remove(key: String): Boolean = g_variant_dict_remove(gVariantDictPtr, key) == TRUE

    public actual operator fun contains(key: String): Boolean = g_variant_dict_contains(gVariantDictPtr, key) == TRUE

    override fun close() {
        g_variant_dict_unref(gVariantDictPtr)
    }
}
