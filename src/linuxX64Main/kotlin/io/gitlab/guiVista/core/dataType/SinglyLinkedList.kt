package io.gitlab.guiVista.core.dataType

import glib2.*
import kotlinx.cinterop.*
import io.gitlab.guiVista.core.Closable
import io.gitlab.guiVista.core.fetchEmptyDataPointer

public actual class SinglyLinkedList(ptr: CPointer<GSList>? = null) : Closable {
    public val gSListPtr: CPointer<GSList>? = ptr ?: createFreshList()
    public actual val length: UInt
        get() = g_slist_length(gSListPtr)
    public var data: CPointer<*>?
        get() = gSListPtr?.pointed?.data
        set(value) {
            gSListPtr?.pointed?.data = value
        }
    public actual val next: SinglyLinkedList?
        get() {
            val ptr = gSListPtr?.pointed?.next
            return if (ptr != null) SinglyLinkedList(ptr) else null
        }
    public actual val last: SinglyLinkedList?
        get() {
            val ptr = g_slist_last(gSListPtr)
            return if (ptr != null) SinglyLinkedList(ptr) else null
        }

    private fun createFreshList(): CPointer<GSList>? {
        var ptr = g_slist_alloc()
        ptr = g_slist_remove(ptr, null)
        return ptr
    }

    /**
     * Adds a new element on to the end of the list. The return value is the new start of the list, which may have
     * changed so make sure you store the new value. Note that the entire list needs to be traversed to find the end,
     * which is inefficient when adding multiple elements. A common idiom to avoid the inefficiency is to prepend the
     * elements, and reverse the list when all elements have been added.
     * @param data The data for the new element.
     * @return The new [SinglyLinkedList] containing the appended [data].
     */
    public infix fun append(data: CPointer<*>?): SinglyLinkedList = fromPointer(g_slist_append(gSListPtr, data))

    /**
     * Removes an element from a list. If two elements contain the same data, only the first is removed. If none of the
     * elements contain the data the list is unchanged.
     * @param data The data of the element to remove.
     * @return The new [SinglyLinkedList] without the [data].
     */
    public infix fun remove(data: CPointer<*>?): SinglyLinkedList = fromPointer(g_slist_remove(gSListPtr, data))

    /**
     * Adds a new element on to the start of the list. The return value is the new start of the list, which may have
     * changed so make sure you store the new value.
     * @param data The data for the new element.
     * @return The new [SinglyLinkedList] containing the prepended [data].
     */
    public infix fun prepend(data: CPointer<*>?): SinglyLinkedList = fromPointer(g_slist_prepend(gSListPtr, data))

    /**
     * Frees up the [SinglyLinkedList] instance. Note that only the list is freed. This function **MUST** be called to
     * prevent memory leaks when you are finished with the [SinglyLinkedList] instance.
     */
    override fun close() {
        if (gSListPtr != null) g_slist_free(gSListPtr)
    }

    /**
     * Removes all list nodes with data equal to data. Returns the new head of the list. Contrast with [remove], which
     * removes only the first node matching the given data.
     * @param data The data to remove.
     * @return The new [SinglyLinkedList] with all instances of [data] removed.
     */
    public infix fun removeAll(data: CPointer<*>?): SinglyLinkedList = fromPointer(g_slist_remove_all(gSListPtr, data))

    /**
     * Inserts a new element into the list at the given position.
     * @param data The data for the new element.
     * @param position The position to insert the element. If this is negative, or is larger than the number of
     * elements in the list, then the new element is added on to the end of the list.
     * @return The new [SinglyLinkedList] containing the [data].
     */
    public fun insert(data: CPointer<*>?, position: Int): SinglyLinkedList =
        fromPointer(g_slist_insert(list = gSListPtr, data = data, position = position))

    /**
     * Inserts a node before sibling containing the [data].
     * @param sibling The node to insert before [data].
     * @param data The data to put in the newly inserted node.
     * @return The new [SinglyLinkedList] containing the [data].
     */
    public fun <E : CPointed> insertBefore(sibling: SinglyLinkedList, data: CPointer<E>?): SinglyLinkedList =
        fromPointer(g_slist_insert_before(slist = gSListPtr, sibling = sibling.gSListPtr, data = data))

    public actual fun reverse(): SinglyLinkedList = fromPointer(g_slist_reverse(gSListPtr))

    /**
     * Finds the element in a [SinglyLinkedList] which contains the given data.
     * @param data The element data to find.
     * @return A value of *true* if the element is found.
     * @see find
     */
    public operator fun contains(data: CPointer<*>?): Boolean = find(data) != null

    /**
     * Finds the element in the list which contains the given data.
     * @param data The element data to find.
     * @return The found list element, or *null* if it isn't found.
     */
    public fun find(data: CPointer<*>?): SinglyLinkedList? {
        val tmp = g_slist_find(gSListPtr, data)
        return if (tmp != null) SinglyLinkedList(tmp) else null
    }

    public actual fun position(listLink: SinglyLinkedList): Int = g_slist_position(gSListPtr, listLink.gSListPtr)

    /**
     * Gets the position of the element containing the given data (starting from *0*).
     * @param data The data to find.
     * @return The index of the element containing the data, or *-1* if the data isn't found.
     */
    public fun index(data: CPointer<*>?): Int = g_slist_index(gSListPtr, data)

    /**
     * Inserts a new element into the list using the given comparison [function][func] to determine its position.
     * @param data The data for the new element.
     * @param func The function to compare elements in the list. It should return a number > *0* if the first parameter
     * comes after the second parameter in the sort order.
     * @return The new sorted [SinglyLinkedList].
     */
    public fun insertSorted(data: gpointer, func: GCompareFunc): SinglyLinkedList =
        fromPointer(g_slist_insert_sorted(list = gSListPtr, data = data, func = func))

    /**
     * Removes an element from a [SinglyLinkedList] without freeing the element. The removed element's next link is set
     * to *null* so that it becomes a self contained list with one element. Removing arbitrary nodes from a singly
     * linked list requires time that is proportional to the length of the list (ie. O(n)). If you find yourself using
     * [removeLink] frequently, you should consider a different data structure such as the
     * [doubly linked list][DoublyLinkedList].
     * @param link An element in the list.
     * @return The new [SinglyLinkedList] without the [link].
     */
    public fun removeLink(link: CPointer<GSList>?): SinglyLinkedList =
        fromPointer(g_slist_remove_link(gSListPtr, link))

    /**
     * Removes the node [link] from the list and frees it. Compare this to [removeLink] which removes the node without
     * freeing it. Removing arbitrary nodes from a singly linked list requires time that is proportional to the length
     * of the list (ie. O(n)). If you find yourself using [deleteLink] frequently, you should consider a different data
     * structure such as the [doubly linked list][DoublyLinkedList].
     * @param link Node to delete.
     * @return The new [SinglyLinkedList] without the [link].
     */
    public fun deleteLink(link: CPointer<GSList>?): SinglyLinkedList =
        fromPointer(g_slist_delete_link(gSListPtr, link))

    /**
     * Copies a singly linked list. Note that this is a **shallow** copy. If the list elements consist of pointers to
     * data, the pointers are copied but the actual data isn't.
     * @return A shallow copy of the list.
     * @see copyDeep
     */
    public actual fun copy(): SinglyLinkedList = fromPointer(g_slist_copy(gSListPtr))

    /**
     * Makes a full (deep) copy of a [SinglyLinkedList]. In contrast with [copy], this function uses [func] to make a
     * copy of each list element, in addition to copying the list container itself.
     * @param func A copy function used to copy every element in the list. Takes two arguments, the data to be copied,
     * and a userData pointer. On common processor architectures it's safe to pass *null* as userData, if the copy
     * function takes only one argument.
     * @param userData The user data passed to [func].
     * @return A full copy of the list. Use [closeAll] to free it.
     */
    public fun copyDeep(func: GCopyFunc, userData: gpointer = fetchEmptyDataPointer()): SinglyLinkedList =
        fromPointer(g_slist_copy_deep(list = gSListPtr, user_data = userData, func = func))

    /**
     * Convenience method which frees all the memory used by a singly linked list, and calls the specified close
     * function on every element's data. The [closeFunc] parameter must **NOT** modify the list (eg, by removing the
     * freed element from it).
     * @param closeFunc The function to be called to free each element's data.
     */
    public fun closeAll(closeFunc: GDestroyNotify) {
        g_slist_free_full(gSListPtr, closeFunc)
    }

    /**
     * Inserts a new element into the list using the given comparison function to determine its position.
     * @param data The data for the new element.
     * @param func The function to compare elements in the list. It should return a number > *0* if the first parameter
     * comes after the second parameter in the sort order.
     * @param userData Data to pass to comparison function.
     * @return The new sorted [SinglyLinkedList].
     */
    public fun insertSortedWithData(
        data: gpointer,
        func: GCompareDataFunc,
        userData: gpointer = fetchEmptyDataPointer()
    ): SinglyLinkedList =
        fromPointer(g_slist_insert_sorted_with_data(list = gSListPtr, data = data, func = func, user_data = userData))

    /**
     * Sorts a singly linked list using the given comparison function. The algorithm used is a stable sort.
     * @param compareFunc The comparison function used to sort the list. This function is passed the data from 2
     * elements of the list and should return *0* if they are equal, a negative value if the first element comes before
     * the second, or a positive value if the first element comes after the second.
     * @return The new sorted [SinglyLinkedList].
     */
    public fun sort(compareFunc: GCompareFunc): SinglyLinkedList = fromPointer(g_slist_sort(gSListPtr, compareFunc))

    /**
     * Like [sort] but this function accepts a [user data argument][userData].
     * @param compareFunc Comparision function.
     * @param userData Data to pass to comparison function.
     */
    public fun sortWithData(
        compareFunc: GCompareDataFunc,
        userData: gpointer = fetchEmptyDataPointer()
    ): SinglyLinkedList =
        fromPointer(g_slist_sort_with_data(list = gSListPtr, compare_func = compareFunc, user_data = userData))

    public actual infix fun concat(list: SinglyLinkedList): SinglyLinkedList =
        fromPointer(g_slist_concat(gSListPtr, list.gSListPtr))

    public actual infix fun elementAt(pos: UInt): SinglyLinkedList? {
        val tmp = g_slist_nth(gSListPtr, pos)
        return if (tmp != null) SinglyLinkedList(tmp) else null
    }

    /**
     * Gets the data of the element at the given [position][pos].
     * @param pos The position of the element.
     * @return The element's data, or *null* if the position is off the end of this list.
     */
    public infix fun dataAt(pos: UInt): gpointer? = g_slist_nth_data(gSListPtr, pos)

    /**
     * Finds an element in this list using a supplied function to find the desired element. It iterates over the list,
     * calling the given function which should return *0* when the desired element is found.
     * @param func The function to call for each element. It should return *0* when the desired element is found. The
     * function takes two COpaquePointer arguments, the list element's data as the first argument, and the given
     * [user data][userData].
     * @param userData The user data passed to [func].
     * @return A [SinglyLinkedList] if it is found, or *null*.
     */
    public fun findCustom(func: GCompareFunc, userData: gpointer = fetchEmptyDataPointer()): SinglyLinkedList? {
        val tmp = g_slist_find_custom(list = gSListPtr, data = userData, func = func)
        return if (tmp != null) SinglyLinkedList(tmp) else null
    }

    /**
     * Calls a [function][func] for each element of a [SinglyLinkedList]. It is safe for [func] to remove the element
     * from the list, but it must **NOT** modify any part of the list after that element.
     * @param func The function to call with each element's data.
     * @param userData The user data to pass to [func].
     */
    public fun iterateElements(func: GFunc, userData: gpointer = fetchEmptyDataPointer()) {
        g_slist_foreach(list = gSListPtr, func = func, user_data = userData)
    }

    public actual companion object {
        public actual fun create(): SinglyLinkedList = SinglyLinkedList()

        public fun fromPointer(ptr: CPointer<GSList>?): SinglyLinkedList = SinglyLinkedList(ptr)
    }
}
