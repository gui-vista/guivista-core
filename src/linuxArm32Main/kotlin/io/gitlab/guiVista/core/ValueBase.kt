package io.gitlab.guiVista.core

import glib2.*
import io.gitlab.guiVista.core.dataType.TypeInfo
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.pointed
import kotlinx.cinterop.toKString

public actual interface ValueBase : Closable {
    public val gValuePtr: CPointer<GValue>
    public actual val typeInfo: TypeInfo
        get() = TypeInfo(gValuePtr.pointed.g_type.toULong())

    public actual fun changeString(newValue: String) {
        g_value_set_string(gValuePtr, newValue)
    }

    public actual fun changeStaticString(newValue: String) {
        g_value_set_static_string(gValuePtr, newValue)
    }

    public actual fun changeBoolean(newValue: Boolean) {
        g_value_set_boolean(gValuePtr, if (newValue) TRUE else FALSE)
    }

    public actual fun changeStaticChar(newValue: Char) {
        g_value_set_schar(gValuePtr, newValue.toByte())
    }

    public actual fun changeDouble(newValue: Double) {
        g_value_set_double(gValuePtr, newValue)
    }

    public actual fun changeFloat(newValue: Float) {
        g_value_set_float(gValuePtr, newValue)
    }

    public actual fun changeInt(newValue: Int) {
        g_value_set_int(gValuePtr, newValue)
    }

    public actual fun changeLong(newValue: Long) {
        g_value_set_long(gValuePtr, newValue.toInt())
    }

    public actual fun changeObject(newValue: Object?) {
        g_value_set_object(gValuePtr, newValue?.objPtr)
    }

    public actual fun changeUByte(newValue: UByte) {
        g_value_set_uchar(gValuePtr, newValue)
    }

    public actual fun changeUInt(newValue: UInt) {
        g_value_set_uint(gValuePtr, newValue)
    }

    public actual fun changeULong(newValue: ULong) {
        g_value_set_ulong(gValuePtr, newValue.toUInt())
    }

    public fun changePointer(newValue: gpointer?) {
        g_value_set_pointer(gValuePtr, newValue)
    }

    public actual fun fetchString(): String = g_value_get_string(gValuePtr)?.toKString() ?: ""

    public actual fun fetchBoolean(): Boolean = g_value_get_boolean(gValuePtr) == TRUE

    public actual fun fetchChar(): Char = g_value_get_char(gValuePtr).toChar()

    public actual fun fetchDouble(): Double = g_value_get_double(gValuePtr)

    public actual fun fetchFloat(): Float = g_value_get_float(gValuePtr)

    public actual fun fetchInt(): Int = g_value_get_int(gValuePtr)

    public actual fun fetchLong(): Long = g_value_get_long(gValuePtr).toLong()

    public actual fun fetchObject(): Object = Object.fromPointer(g_value_get_object(gValuePtr))

    public actual fun fetchUByte(): UByte = g_value_get_uchar(gValuePtr)

    public actual fun fetchUInt(): UInt = g_value_get_uint(gValuePtr)

    public actual fun fetchULong(): ULong = g_value_get_ulong(gValuePtr).toULong()

    public fun fetchPointer(): gpointer? = g_value_get_pointer(gValuePtr)

    override fun close() {
        g_value_unset(gValuePtr)
    }
}
