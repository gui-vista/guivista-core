package io.gitlab.guiVista.core

import glib2.*
import kotlinx.cinterop.reinterpret

public actual class Object private constructor(ptr: gpointer?) : ObjectBase {
    public val objPtr: gpointer? = ptr

    /**
     * Creates a binding between [sourceProperty] on this Object and [targetProperty] on [targetObj]. Whenever the
     * [sourceProperty] is changed the [targetProperty] is updated using the same value.
     *
     * The binding will automatically be removed when either the source or the target objects are closed. To remove
     * the binding without affecting the source, and the target objects you can just call [DataBinding.close] on the
     * returned [DataBinding] instance.
     * @param sourceProperty The property on this [Object] to bind.
     * @param targetObj The target Object.
     * @param targetProperty The property on the [targetObj] to bind.
     * @param biDirectional If set to *true* then a change made to [sourceProperty] updates [targetProperty], and vice
     * versa.
     * @return An instance of [DataBinding] that represents the data binding between two objects.
     */
    public fun bindProperty(
        sourceProperty: String,
        targetObj: gpointer?,
        targetProperty: String,
        biDirectional: Boolean = false
    ): DataBinding {
        val ptr = g_object_bind_property(
            source = objPtr,
            source_property = sourceProperty,
            target = targetObj,
            target_property = targetProperty,
            flags = if (biDirectional) G_BINDING_BIDIRECTIONAL else G_BINDING_DEFAULT
        )
        return DataBinding.fromPointer(ptr)
    }

    /**
     * Creates a binding between [sourceProperty] on this Object and [targetProperty] on [targetObj], allowing you to
     * set the transformation functions to be used by the binding.
     *
     * The binding will automatically be removed when either the source or the [target][targetObj] instances are
     * closed. To remove the binding without affecting the source and the target you can just call [DataBinding.close]
     * on the returned [DataBinding] instance. An Object can have multiple bindings. The same [userData] parameter will
     * be used for both [transformToFunc], and [transformFromFunc] functions; the [notifyFunc] function will be called
     * once, when the binding is removed.
     * @param sourceProperty The property on this [Object] to bind.
     * @param targetObj The target Object.
     * @param targetProperty The property on the [targetObj] to bind.
     * @param biDirectional If set to *true* then a change made to [sourceProperty] updates [targetProperty], and vice
     * versa.
     * @param transformToFunc The transformation function from the source to the [target][targetObj], or *null* to use
     * the default.
     * @param transformFromFunc The transformation function from the [target][targetObj] to the source, or *null* to
     * use the default.
     * @param userData Custom data to be passed to the transformation functions or *null*.
     * @param notifyFunc The function to be called when disposing the binding, and to free the resources used by the
     * transformation functions.
     * @return An instance of [DataBinding] that represents the data binding between two objects.
     */
    public fun bindProperty(
        sourceProperty: String,
        targetObj: gpointer?,
        targetProperty: String,
        biDirectional: Boolean = false,
        transformToFunc: GBindingTransformFunc,
        transformFromFunc: GBindingTransformFunc,
        userData: gpointer = fetchEmptyDataPointer(),
        notifyFunc: GDestroyNotify
    ): DataBinding {
        val ptr = g_object_bind_property_full(
            source = objPtr,
            source_property = sourceProperty,
            target = targetObj,
            target_property = targetProperty,
            flags = if (biDirectional) G_BINDING_BIDIRECTIONAL else G_BINDING_DEFAULT,
            user_data = userData,
            transform_to = transformToFunc,
            transform_from = transformFromFunc,
            notify = notifyFunc
        )
        return DataBinding.fromPointer(ptr)
    }

    public actual fun fetchProperty(name: String, type: ULong): ValueBase {
        val result = Value.create(type)
        g_object_get_property(`object` = objPtr?.reinterpret(), property_name = name, value = result.gValuePtr)
        return result
    }

    public actual fun changeBooleanProperty(name: String, value: Boolean) {
        val valueObj = Value.create(G_TYPE_BOOLEAN.toULong()).apply { changeBoolean(value) }
        g_object_set_property(`object` = objPtr?.reinterpret(), property_name = name, value = valueObj.gValuePtr)
        valueObj.close()
    }

    public actual fun changeStaticCharProperty(name: String, value: Char) {
        val valueObj = Value.create(G_TYPE_CHAR.toULong()).apply { changeStaticChar(value) }
        g_object_set_property(`object` = objPtr?.reinterpret(), property_name = name, value = valueObj.gValuePtr)
        valueObj.close()
    }

    public actual fun changeDoubleProperty(name: String, value: Double) {
        val valueObj = Value.create(G_TYPE_DOUBLE.toULong()).apply { changeDouble(value) }
        g_object_set_property(`object` = objPtr?.reinterpret(), property_name = name, value = valueObj.gValuePtr)
        valueObj.close()
    }

    public actual fun changeFloatProperty(name: String, value: Float) {
        val valueObj = Value.create(G_TYPE_FLOAT.toULong()).apply { changeFloat(value) }
        g_object_set_property(`object` = objPtr?.reinterpret(), property_name = name, value = valueObj.gValuePtr)
        valueObj.close()
    }

    public actual fun changeIntProperty(name: String, value: Int) {
        val valueObj = Value.create(G_TYPE_INT.toULong()).apply { changeInt(value) }
        g_object_set_property(`object` = objPtr?.reinterpret(), property_name = name, value = valueObj.gValuePtr)
        valueObj.close()
    }

    public actual fun changeLongProperty(name: String, value: Long) {
        val valueObj = Value.create(G_TYPE_LONG.toULong()).apply { changeLong(value) }
        g_object_set_property(`object` = objPtr?.reinterpret(), property_name = name, value = valueObj.gValuePtr)
        valueObj.close()
    }

    public actual fun changeObjectProperty(name: String, value: Object?) {
        val valueObj = Value.create(G_TYPE_OBJECT.toULong()).apply { changeObject(value) }
        g_object_set_property(`object` = objPtr?.reinterpret(), property_name = name, value = valueObj.gValuePtr)
        valueObj.close()
    }

    public actual fun changeUByteProperty(name: String, value: UByte) {
        val valueObj = Value.create(G_TYPE_UCHAR.toULong()).apply { changeUByte(value) }
        g_object_set_property(`object` = objPtr?.reinterpret(), property_name = name, value = valueObj.gValuePtr)
        valueObj.close()
    }

    public actual fun changeUIntProperty(name: String, value: UInt) {
        val valueObj = Value.create(G_TYPE_UINT.toULong()).apply { changeUInt(value) }
        g_object_set_property(`object` = objPtr?.reinterpret(), property_name = name, value = valueObj.gValuePtr)
        valueObj.close()
    }

    public actual fun changeULongProperty(name: String, value: ULong) {
        val valueObj = Value.create(G_TYPE_ULONG.toULong()).apply { changeULong(value) }
        g_object_set_property(`object` = objPtr?.reinterpret(), property_name = name, value = valueObj.gValuePtr)
        valueObj.close()
    }

    public actual fun changeStaticStringProperty(name: String, value: String) {
        val valueObj = Value.create(G_TYPE_STRING.toULong()).apply { changeStaticString(value) }
        g_object_set_property(`object` = objPtr?.reinterpret(), property_name = name, value = valueObj.gValuePtr)
    }

    public companion object {
        public fun fromPointer(ptr: gpointer?): Object = Object(ptr)
    }

    override fun close() {
        g_object_unref(objPtr)
    }
}
