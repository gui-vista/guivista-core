package io.gitlab.guiVista.core.dataType

import glib2.*
import kotlinx.cinterop.COpaquePointer
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.pointed
import kotlinx.cinterop.reinterpret
import io.gitlab.guiVista.core.Closable
import io.gitlab.guiVista.core.fetchEmptyDataPointer

public actual class DoublyLinkedList private constructor(ptr: CPointer<GList>? = null) : Closable {
    public val gListPtr: CPointer<GList>? = ptr ?: createFreshList()
    public actual val length: UInt
        get() = g_list_length(gListPtr)
    public var data: CPointer<*>?
        get() = gListPtr?.pointed?.data
        set(value) {
            gListPtr?.pointed?.data = value
        }
    public actual val next: DoublyLinkedList?
        get() {
            val tmp = gListPtr?.pointed
            return if (tmp != null) DoublyLinkedList(tmp.next?.reinterpret()) else null
        }
    public actual val prev: DoublyLinkedList?
        get() {
            val tmp = gListPtr?.pointed
            return if (tmp != null) DoublyLinkedList(tmp.prev?.reinterpret()) else null
        }
    public actual val first: DoublyLinkedList?
        get() {
            val tmp = g_list_first(gListPtr)
            return if (tmp != null) DoublyLinkedList(tmp) else null
        }
    public actual val last: DoublyLinkedList?
        get() {
            val tmp = g_list_last(gListPtr)
            return if (tmp != null) DoublyLinkedList(tmp) else null
        }

    private fun createFreshList(): CPointer<GList>? {
        var ptr = g_list_alloc()
        ptr = g_list_remove(ptr, null)
        return ptr
    }

    /**
     * Frees up the [DoublyLinkedList] instance. Note that only the list is freed. This function **MUST** be called to
     * prevent memory leaks when you are finished with the [DoublyLinkedList] instance.
     */
    override fun close() {
        if (gListPtr != null) g_list_free(gListPtr)
    }

    /**
     * Adds a new element on to the end of the list. The return value is the new start of the list, which may have
     * changed so make sure you store the new value. Note that the entire list needs to be traversed to find the end,
     * which is inefficient when adding multiple elements. A common idiom to avoid the inefficiency is to prepend the
     * elements, and reverse the list when all elements have been added.
     * @param data The data for the new element.
     * @return The new [DoublyLinkedList] containing the appended element.
     */
    public infix fun append(data: CPointer<*>?): DoublyLinkedList = fromPointer(g_list_append(gListPtr, data))

    /**
     * Removes an element from a list. If two elements contain the same data, only the first is removed. If none of the
     * elements contain the data the list is unchanged.
     * @param data The data of the element to remove.
     * @return A new [DoublyLinkedList] with the element removed.
     */
    public infix fun remove(data: CPointer<*>?): DoublyLinkedList = fromPointer(g_list_remove(gListPtr, data))

    /**
     * Adds a new element on to the start of the list. The return value is the new start of the list, which may have
     * changed so make sure you store the new value.
     * @param data The data for the new element.
     * @return A new [DoublyLinkedList] containing the prepended element.
     */
    public infix fun prepend(data: CPointer<*>?): DoublyLinkedList = fromPointer(g_list_prepend(gListPtr, data))

    /**
     * Removes all list nodes with data equal to data. Returns the new head of the list. Contrast with [remove], which
     * removes only the first node matching the given data.
     * @param data The data to remove.
     * @return A new [DoublyLinkedList] with all instances of [data] removed.
     */
    public infix fun removeAll(data: CPointer<*>?): DoublyLinkedList = fromPointer(g_list_remove_all(gListPtr, data))

    /**
     * Inserts a new element into the list at the given position.
     * @param data The data for the new element.
     * @param position The position to insert the element. If this is negative, or is larger than the number of
     * elements in the list, then the new element is added on to the end of the list.
     * @return A new [DoublyLinkedList] containing the inserted [data].
     */
    public fun insert(data: CPointer<*>?, position: Int): DoublyLinkedList =
        fromPointer(g_list_insert(list = gListPtr, data = data, position = position))

    /**
     * Inserts a node before sibling containing data.
     * @param sibling A node to insert data before.
     * @param data The data to put in the newly inserted node.
     * @return A new [DoublyLinkedList] containing the inserted node.
     */
    public fun insertBefore(sibling: DoublyLinkedList, data: CPointer<*>?): DoublyLinkedList =
        fromPointer(g_list_insert_before(list = gListPtr, sibling = sibling.gListPtr, data = data))

    public actual fun reverse(): DoublyLinkedList = fromPointer(g_list_reverse(gListPtr))

    /**
     * Checks to see if [data] exists in the [DoublyLinkedList].
     * @param data The element data to find.
     * @return A value of *true* if [data] is in the [DoublyLinkedList].
     * @see find
     */
    public operator fun contains(data: CPointer<*>?): Boolean = find(data) != null

    /**
     * Finds the element in a [DoublyLinkedList] which contains the given data.
     * @param data The element data to find.
     * @return The found list element, or *null* if it isn't found.
     */
    public fun find(data: CPointer<*>?): DoublyLinkedList? {
        val tmp = g_list_find(gListPtr, data)
        return if (tmp != null) DoublyLinkedList(tmp) else null
    }

    public actual fun position(listLink: DoublyLinkedList): Int = g_list_position(gListPtr, listLink.gListPtr)

    /**
     * Gets the position of the element containing the given data (starting from 0).
     * @param data The data to find.
     * @return The index of the element containing the data, or *-1* if the data isn't found.
     */
    public fun index(data: CPointer<*>?): Int = g_list_index(gListPtr, data)

    /**
     * Inserts a new element into the list using the given comparison function to determine its position. If you are
     * adding many new elements to a list, and the number of new elements is much larger than the length of the list,
     * then use [prepend] to add the new items, and sort the list afterwards with [sort].
     * @param data The data for the new element.
     * @param func The function to compare elements in the list. It should return a number > *0* if the first parameter
     * comes after the second parameter in the sort order.
     * @return A new [DoublyLinkedList] containing the inserted [data].
     */
    public fun insertSorted(data: gpointer, func: GCompareFunc): DoublyLinkedList =
        fromPointer(g_list_insert_sorted(list = gListPtr, data = data, func = func))

    public actual infix fun removeLink(link: DoublyLinkedList): DoublyLinkedList =
        fromPointer(g_list_remove_link(gListPtr, link.gListPtr))

    public actual infix fun deleteLink(link: DoublyLinkedList): DoublyLinkedList =
        fromPointer(g_list_delete_link(gListPtr, link.gListPtr))

    /**
     * Removes all list nodes with data equal to [data]. Returns the new head of the list. Contrast with [remove] which
     * removes only the first node matching the given data.
     * @param data Data to remove.
     * @return A new [DoublyLinkedList] with all instances of [data] removed.
     */
    public infix fun removeAll(data: COpaquePointer): DoublyLinkedList = fromPointer(g_list_remove_all(gListPtr, data))

    /**
     * Convenience method which frees all the memory used by this list, and calls free_func on every element's data.
     * **Note:** *free_func* must **NOT** modify the list (eg, by removing the freed element from it).
     * @param freeFunc The function to be called to free each element's data.
     */
    public fun closeAll(freeFunc: GDestroyNotify) {
        g_list_free_full(gListPtr, freeFunc)
    }

    public actual fun copy(): DoublyLinkedList = DoublyLinkedList(g_list_copy(gListPtr))

    /**
     * Makes a full (deep) copy of this list. In contrast with [copy], this function uses func to make a copy of each
     * list element, in addition to copying the list container itself.
     * @param func A copy function used to copy every element in the list. This function takes two arguments, the data
     * to be copied and a [userData] pointer. On common processor architectures it's safe to pass *null* as [userData]
     * if the copy function takes only one argument.
     * @param userData User data passed to [func].
     * @return The start of the new list that holds a full copy of this list. Use [closeAll] to free it.
     */
    public fun copyDeep(func: GCopyFunc, userData: gpointer = fetchEmptyDataPointer()): DoublyLinkedList =
        DoublyLinkedList(g_list_copy_deep(list = gListPtr, func = func, user_data = userData))

    /**
     * Sorts this list using the given comparison function. The algorithm used is a stable sort.
     * @param compareFunc The comparison function used to sort this list. This function is passed the data from two
     * elements of this list, and should return *0* if they are equal, a negative value if the first element comes
     * before the second, or a positive value if the first element comes after the second.
     * @return A new sorted [DoublyLinkedList].
     */
    public fun sort(compareFunc: GCompareFunc): DoublyLinkedList = fromPointer(g_list_sort(gListPtr, compareFunc))

    /**
     * Inserts a new element into this list, using the given comparison function to determine its position. If you are
     * adding many new elements to a list, and the number of new elements is much larger than the length of the list,
     * then use [prepend] to add the new items, and sort the list afterwards with [sort].
     * @param data The data for the new element.
     * @param func The function to compare elements in this list. It should return a number > *0* if the first
     * parameter comes after the second parameter in the sort order.
     * @param userData User data to pass to [func].
     * @return A new sorted [DoublyLinkedList].
     */
    public fun insertSortedWithData(
        data: gpointer,
        func: GCompareDataFunc,
        userData: gpointer = fetchEmptyDataPointer()
    ): DoublyLinkedList =
        fromPointer(g_list_insert_sorted_with_data(list = gListPtr, func = func, data = data, user_data = userData))

    /**
     * Like [sort] but the comparison function accepts a [user data][userData] argument.
     * @param compareFunc Comparison function.
     * @param userData User data to pass to [compareFunc].
     * @return A new sorted [DoublyLinkedList].
     */
    public fun sortWithData(
        compareFunc: GCompareDataFunc,
        userData: gpointer = fetchEmptyDataPointer()
    ): DoublyLinkedList =
        fromPointer(g_list_sort_with_data(list = gListPtr, compare_func = compareFunc, user_data = userData))

    public actual infix fun concat(list: DoublyLinkedList): DoublyLinkedList =
        fromPointer(g_list_concat(gListPtr, list.gListPtr))

    public actual infix fun elementAt(pos: UInt): DoublyLinkedList? {
        val tmp = g_list_nth(gListPtr, pos)
        return if (tmp != null) DoublyLinkedList(tmp) else null
    }

    /**
     * Gets the data of the element at the given position. This iterates over the list until it reaches the n -th
     * position. If you intend to iterate over every element, it is better to use the [iterateElements] function.
     * @param pos The position of the element.
     * @return The element's data, or *null* if the position is off the end of this list.
     */
    public infix fun dataAt(pos: UInt): gpointer? = g_list_nth_data(gListPtr, pos)

    /**
     * Finds an element in this list using a supplied function to find the desired element. It iterates over the list,
     * calling the given function which should return *0* when the desired element is found.
     * @param func The function to call for each element. It should return *0* when the desired element is found. The
     * function takes two COpaquePointer arguments, the list element's data as the first argument, and the given
     * [user data][userData].
     * @param userData User data passed to the [function][func].
     * @return The found doubly linked list element, or *null* if it isn't found.
     */
    public fun findCustom(func: GCompareFunc, userData: gpointer = fetchEmptyDataPointer()): DoublyLinkedList? {
        val tmp = g_list_find_custom(list = gListPtr, data = userData, func = func)
        return if (tmp != null) DoublyLinkedList(tmp) else null
    }

    /**
     * Calls a [function][func] for each element of a [DoublyLinkedList]. It is safe for [func] to remove the element
     * from the list, but it must **NOT** modify any part of the list after that element.
     * @param func The function to call with each element's data.
     * @param userData The user data to pass to [func].
     */
    public fun iterateElements(func: GFunc, userData: gpointer = fetchEmptyDataPointer()) {
        g_list_foreach(list = gListPtr, func = func, user_data = userData)
    }

    public actual fun elementAtPrevious(pos: UInt): DoublyLinkedList? {
        val ptr = g_list_nth_prev(gListPtr, pos)
        return if (ptr != null) fromPointer(ptr) else null
    }

    public actual companion object {
        public fun fromPointer(ptr: CPointer<GList>?): DoublyLinkedList = DoublyLinkedList(ptr)

        public actual fun create(): DoublyLinkedList = DoublyLinkedList()
    }
}
