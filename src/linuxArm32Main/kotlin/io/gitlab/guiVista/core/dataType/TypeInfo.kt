package io.gitlab.guiVista.core.dataType

import glib2.*
import io.gitlab.guiVista.core.Quark
import kotlinx.cinterop.toKString

public actual inline class TypeInfo(public val id: ULong) {
    public actual fun fetchName(): String = g_type_name(id.toUInt())?.toKString() ?: ""

    public actual fun fetchQuarkName(): Quark = Quark(g_type_qname(id.toUInt()))

    /**
     * Attaches arbitrary data to a typeInfo.
     * @param quark The [Quark] to identify the data.
     * @param data The data to use.
     */
    public fun changeQuarkData(quark: Quark, data: gpointer?) {
        g_type_set_qdata(type = id.toUInt(), quark = quark.id, data = data)
    }

    /**
     * Obtains data which has previously been attached to this typeInfo with [changeQuarkData]. Note that this does not
     * take subtyping into account. Data attached to one typeInfo with [changeQuarkData] **CANNOT** be retrieved from a
     * subtype using [fetchQuarkData].
     * @param quark The [Quark] to identify the data.
     * @return The data or *null* if no data was found.
     */
    public fun fetchQuarkData(quark: Quark): gpointer? = g_type_get_qdata(id.toUInt(), quark.id)

    public actual companion object {
        public actual fun fromName(name: String): TypeInfo = TypeInfo(g_type_from_name(name).toULong())
    }
}
