package io.gitlab.guiVista.core

import glib2.g_quark_from_static_string
import glib2.g_quark_from_string
import glib2.g_quark_try_string

public actual inline class Quark(public val id: UInt) {
    public actual companion object {
        public actual fun fromString(string: String): Quark = Quark(g_quark_from_string(string))

        public actual fun fromStaticString(string: String): Quark = Quark(g_quark_from_static_string(string))

        public actual fun tryString(string: String): Quark = Quark(g_quark_try_string(string))
    }
}
