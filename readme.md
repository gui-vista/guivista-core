# GUI Vista Core (guivista-core)

A Kotlin Native library that provides core functionality, which is based on the
[GLib](https://developer.gnome.org/glib/) library v2.48 (required by the GUI Vista Core library). This library depends
on Kotlin Native (requires Kotlin 1.4.31) which is currently in beta, and doesn't provide any backwards compatibility
guarantees!


## Setup Gradle Build File

In order to use the library with Gradle (version 6.7.1 or higher) do the following:

1. Open/create a Kotlin Native project which targets **linuxX64** or **linuxArm32Hfp**
2. Open the project's **build.gradle.kts** file
3. Insert the following into the **repositories** block:
```kotlin
mavenCentral()
```
4. Create a library definition file called **glib2.def** which contains the following:
```
linkerOpts = -lglib-2.0 -lgobject-2.0
linkerOpts.linux_x64 = -L/usr/lib/x86_64-linux-gnu
linkerOpts.linux_arm32_hfp = -L/mnt/pi_image/usr/lib/arm-linux-gnueabihf
```

5. Add the glib2 library dependency: `cinterops.create("glib2")`
6. Add the GUI Vista Core library dependency: `implementation("io.gitlab.gui-vista:guivista-core:$guiVistaVer")`

The build file should look similar to the following:
```kotlin
// ...
repositories {
    mavenCentral()
}

kotlin {
    // ...
    linuxX64 {
        // ...
        compilations.getByName("main") {
            cinterops.create("glib2")
            dependencies {
                val guiVistaVer = "0.4.3"
                implementation("io.gitlab.gui-vista:guivista-core:$guiVistaVer")
            }
        }
    }
}
```

# Data Types

A number of data types are provided by this library which include the following:

- Variant
- VariantDictionary
- DoublyLinkedList
- SinglyLinkedList

## Variant

A variable data type (eg similar to Kotlin's **Any**) that holds a value. Below is a **String** Variant example:

```kotlin
val strVariant = Variant.fromString("Hello World! :)")
println("String Variant Value: ${strVariant.stringValue}")
strVariant.close()
```

When finished with a Variant the `close` function **MUST** be called to free up resources.

## VariantDictionary

Like a Map of entries where each entry consists of a key and value (similar to Kotlin's **Map** and **MutableMap**).
Below is a VariantDictionary example:

```kotlin
val dict = VariantDictionary.create()
dict.insertEntry("answerToUniverse", Variant.fromUInt(42u))
println("Value From Dictionary: ${dict.lookupValue(key = "answerToUniverse")?.uIntValue}")
dict.close()
```

When finished with a VariantDictionary the `close` function **MUST** be called to free up resources.
