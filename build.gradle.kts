import java.util.Properties
import org.jetbrains.dokka.gradle.DokkaTask

val gitLabSettings = fetchGitLabSettings()
val projectSettings = fetchProjectSettings()

group = "io.gitlab.gui-vista"
version = if (projectSettings.isDevVer) "${projectSettings.libVer}-dev" else projectSettings.libVer

plugins {
    kotlin("multiplatform") version "1.4.31"
    `maven-publish`
    id("org.jetbrains.dokka") version "1.4.30"
    signing
}

ext["signing.keyId"] = null
ext["signing.password"] = null
ext["signing.secretKeyRingFile"] = null
ext["ossrhUsername"] = null
ext["ossrhPassword"] = null

val secretPropsFile = project.rootProject.file("maven_central.properties")
if (secretPropsFile.exists()) {
    secretPropsFile.reader().use { reader ->
        Properties().apply {
            load(reader)
        }
    }.onEach { (name, value) ->
        ext["$name"] = value
    }
} else {
    ext["signing.keyId"] = System.getenv("SIGNING_KEY_ID") ?: ""
    ext["signing.password"] = System.getenv("SIGNING_PASSWORD") ?: ""
    ext["signing.secretKeyRingFile"] = System.getenv("SIGNING_SECRET_KEY_RING_FILE") ?: ""
    ext["ossrhUsername"] = System.getenv("OSSRH_USERNAME") ?: ""
    ext["ossrhPassword"] = System.getenv("OSSRH_PASSWORD") ?: ""
}

repositories {
    mavenCentral()
    // TODO: Remove the repository below.
    jcenter()
}

val dokkaHtml by tasks.getting(DokkaTask::class)

val javadocJar by tasks.register("javadocJar", Jar::class) {
    dependsOn(dokkaHtml)
    archiveClassifier.set("javadoc")
    from(dokkaHtml.outputDirectory)
}

kotlin {
    explicitApi()
    linuxX64("linuxX64") {
        compilations.getByName("main") {
            cinterops.create("glib2") {
                includeDirs("/usr/include/glib-2.0")
            }
        }
    }
    linuxArm32Hfp("linuxArm32") {
        compilations.getByName("main") {
            cinterops.create("glib2") {
                includeDirs("/mnt/pi_image/usr/include/glib-2.0")
            }
        }
    }
    sourceSets {
        val unsignedTypes = "kotlin.ExperimentalUnsignedTypes"
        @Suppress("UNUSED_VARIABLE") val commonMain by getting {
            languageSettings.useExperimentalAnnotation(unsignedTypes)
            dependencies {
                val kotlinVer = "1.4.31"
                implementation(kotlin("stdlib-common", kotlinVer))
            }
        }
        @Suppress("UNUSED_VARIABLE") val linuxX64Main by getting {
            languageSettings.useExperimentalAnnotation(unsignedTypes)
        }
        @Suppress("UNUSED_VARIABLE") val linuxArm32Main by getting {
            languageSettings.useExperimentalAnnotation(unsignedTypes)
        }
        all {
            languageSettings.enableLanguageFeature("InlineClasses")
        }
    }
}

publishing {
    val publishToMavenCentral = getExtraString("mavenCentral.publishingEnabled")?.toBoolean() ?: false
    publications.withType<MavenPublication> {
        if (projectSettings.includeDocs) artifact(javadocJar)
        createPom()
    }
    repositories {
        if (gitLabSettings.publishingEnabled) {
            createGitLabRepo(projectId = gitLabSettings.projectId, token = gitLabSettings.token)
        }
        if (publishToMavenCentral) createMavenCentralRepo()
    }
}

tasks.create("createLinuxLibraries") {
    dependsOn("linuxX64MainKlibrary", "linuxArm32MainKlibrary")
}

tasks.getByName("publish") {
    doFirst { println("Project Version: ${project.version}") }
}

data class GitLabSettings(val token: String, val projectId: Int, val publishingEnabled: Boolean)

fun fetchGitLabSettings(): GitLabSettings {
    var token = ""
    var projectId = -1
    val properties = Properties()
    var publishingEnabled = true
    file("gitlab.properties").inputStream().use { inputStream ->
        properties.load(inputStream)
        publishingEnabled = properties.getProperty("publishingEnabled")?.toBoolean() ?: true
        token = properties.getProperty("token") ?: ""
        @Suppress("RemoveSingleExpressionStringTemplate")
        projectId = "${properties.getProperty("projectId")}".toInt()
    }
    return GitLabSettings(token = token, projectId = projectId, publishingEnabled = publishingEnabled)
}

data class ProjectSettings(val libVer: String, val isDevVer: Boolean, val includeDocs: Boolean)

fun fetchProjectSettings(): ProjectSettings {
    var libVer = "SNAPSHOT"
    var isDevVer = true
    var includeDocs = false
    val properties = Properties()
    file("project.properties").inputStream().use { inputStream ->
        properties.load(inputStream)
        libVer = properties.getProperty("libVer") ?: "SNAPSHOT"
        @Suppress("RemoveSingleExpressionStringTemplate")
        isDevVer = "${properties.getProperty("isDevVer")}".toBoolean()
        @Suppress("RemoveSingleExpressionStringTemplate")
        includeDocs = "${properties.getProperty("includeDocs")}".toBoolean()
    }
    return ProjectSettings(libVer = libVer, isDevVer = isDevVer, includeDocs = includeDocs)
}

val Boolean.intValue: Int
    get() = if (this) 1 else 0

signing {
    sign(publishing.publications)
}

fun getExtraString(name: String) = ext[name]?.toString()

fun MavenPublication.createPom() = pom {
    name.set("GUI Vista Core")
    description.set("A Kotlin Native library that provides core functionality in a Kotlin Native project.")
    url.set("https://gitlab.com/gui-vista/guivista-core")

    licenses {
        license {
            name.set("Apache 2.0")
            url.set("https://opensource.org/licenses/Apache-2.0")
        }
    }
    developers {
        developer {
            id.set("NickApperley")
            name.set("Nick Apperley")
            email.set("napperley@protonmail.com")
        }
    }
    scm {
        url.set("https://gitlab.com/gui-vista/guivista-core")
    }
}

fun RepositoryHandler.createMavenCentralRepo() {
    maven {
        name = "sonatype"
        setUrl("https://s01.oss.sonatype.org/service/local/staging/deploy/maven2/")
        credentials {
            username = getExtraString("ossrhUsername")
            password = getExtraString("ossrhPassword")
        }
    }
}

fun RepositoryHandler.createGitLabRepo(projectId: Int, token: String) {
    maven {
        name = "Git Lab"
        url = uri("https://gitlab.com/api/v4/projects/$projectId/packages/maven")
        credentials(HttpHeaderCredentials::class.java) {
            name = "Private-Token"
            value = token
        }
        authentication {
            create("header", HttpHeaderAuthentication::class.java)
        }
    }
}
